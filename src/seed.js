let colHeaders = ['', '12', '1', '2', '3', '4', '5', '6',
  '7', '8', '9', '10 ', '11', '12', '1', '2', '3', '4',
  '5', '6', '7', '8', '9', '10 ', '11'
];

let seedData = [
  ["S"],
  ["M"],
  ["T"],
  ["W"],
  ["Th"],
  ["F"],
  ["Sa"]
].map(d => [...d, ...new Array(24).fill('**')]);

export const tbsettings = {
  data: seedData,
  colHeaders: colHeaders,
  fixedColumnsLeft: 1,
  stretchH: 'all',
  preventOverflow: 'horizontal',
};
