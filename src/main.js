import 'babel-polyfill'
import 'es6-promise/auto'
import Vue from 'vue';
import App from './App.vue'
import Vuetify from 'vuetify'
import {
    store
}
from './store/index.js'

Vue.use(Vuetify);
new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
