export const mutations = {
  editAvailableLanes(state, availableLanes) {
    state.availableLanes = availableLanes;
    state.aColumn.source = [...state.dropdown, ...[...Array(availableLanes).keys()].slice(1, )];
  }
}
