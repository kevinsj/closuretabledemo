import Handsontable from 'handsontable'
import Vue from 'vue'
import Vuex from 'vuex'
import {
  mutations
} from './mutations'
import actions from './actions'

Vue.use(Vuex)

Handsontable.renderers.registerRenderer('customrenderer', function customrenderer(instance, td, row, col, prop, value, cellProperties) {
  Handsontable.renderers.DropdownRenderer.apply(this, arguments);
  if (value === 1) td.style.backgroundColor = '#FFEB9C';
  else if (value === 2) td.style.backgroundColor = '#C6EFCE';
  else if (value === 3) td.style.backgroundColor = '#7BB0FF';
  else if (value === 4) td.style.backgroundColor = '#6584FF';
  else if (value === 5) td.style.backgroundColor = '#00FFFF';
  else if (value === 'FC') td.style.backgroundColor = '#FFFF00';
  else td.style.backgroundColor = 'pink';
  cellProperties.valid = true;
});

export const store = new Vuex.Store({
  state: {
    availableLanes: 0,
    dropdown: ['**', 'FC'],
    aColumn: {
      type: 'dropdown',
      allowInvalid: true,
      allowEmpty: true,
      source: ['**', 'FC'],
      renderer: 'customrenderer'
    }
  },
  mutations,
  actions
});
